"""
Adventure is a text adventure game
Copyright (C) 2017 Chris Beelby <cmbeelby@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import textwrap


class Player(object):
    def __init__(self, name):
        self.name = name
        self.inventory = []
        self.points = 0


class Intro(object):
    def __init__(self, introText):
        self.introText = introText
    def __str__(self):
        return self.introText


class Item(object):
    def __init__(self, name, aliases):
        self.name = name
        self.aliases = aliases

    def __repr__(self):
        return "Item(name={}, aliases={})".format(
            repr(self.name), repr(self.aliases))

    def __str__(self):
        return self.name


class Room(object):
    def __init__(self, name, long_description):
        self.name = name
        self.long_description = long_description
        self.contents = []
        self.directions = {}

    def get_long_description(self):
        return self.long_description


class Adventure(object):
    def __init__(self, name, start_room):
        self.name = name
        self.player = None
        self.current_room = None
        self.game_over = False
        self.looked = False
        self.start_room = start_room
        self.filter_words = ['a', 'an', 'the', 'it', 'in', 'to']

    @staticmethod
    def wrap_message(message):
        print(textwrap.fill(message, width=55))

    def show_intro(self, introText):
        print(introText)

    def show_full_room_description(self):
        room = self.current_room
        print()
        Adventure.wrap_message(room.get_long_description())

        if room.contents:
            print()
        for item in room.contents:
            print("There is a {} here".format(item.name))
        print()
        print("There are exits {} here.".format(
            str.join(', ', room.directions.keys())))
        self.looked = True

    def start_adventure(self, player, intro):
        self.player = player
        self.show_intro(intro)
        self.current_room = self.start_room
        while not self.game_over:
            if not self.looked:
                self.show_full_room_description()
                print()
            self.process_input()
        self.handle_game_over()

    def handle_game_over(self):
        print("Thanks for playing {}".format(self.player.name))
        print("Your final score was {}".format(self.player.points))

    def process_input(self):
        print("What now? ", end='')

        def filter_word(word):
            return word.lower() in self.filter_words
        words = [x.lower() for x in input().split() if not filter_word(x)]
        if not words:
            return
        verb, *other_words = words
        near_items = self.current_room.contents + self.player.inventory
        item = Adventure.find_item(near_items, other_words)
        m = getattr(self, 'handle_verb_' + verb, None)
        if m:
            m(item, other_words)
        else:
            print("Sorry I did not understand that")

    def handle_verb_go(self, item, words):
        """ Process the intent of the player to go somewhere

        This method represents the "intent" to go in a direction. It takes
        the first word as the direction and looks in the dictionary of possible
        directions for the current room. If the direction is found then the
        go method is called and passed the destination room. Otherwise a
        message is displayed indicating that the direction desired is not
        valid.

        :param item: a matched item. not used.
        :param words: the list of words typed after the "go" command
        :return: None
        """
        if words:
            direction = words[0]
            new_room = self.current_room.directions.get(direction)
            if new_room:
                self.go(new_room)
            else:
                Adventure.wrap_message(
                    "I do not see an exit {} from here".format(direction))
        else:
            print("Go where?")

    def handle_verb_quit(self, item, words):
        if item or words:
            print("Your attempt to quit failed")
        else:
            self.game_over = True

    def handle_verb_get(self, item, _):
        if item and item in self.current_room.contents:
            self.get_item(item)
        elif item:
            print("You already have that")
        else:
            print("Get what?")

    def handle_verb_drop(self, item, _):
        if item and item in self.player.inventory:
            self.drop_item(item)
        elif item:
            print("You are not carrying that")
        else:
            print("Drop what?")

    def handle_verb_inventory(self, item, _):
        if item:
            print("Yes, it is in your inventory")
        else:
            self.show_inventory()
    handle_verb_inv = handle_verb_inventory

    def show_inventory(self):
        if self.player.inventory:
            print("You are carrying the following items:")
            for item in self.player.inventory:
                print("a {}".format(item.name))
        else:
            print("Your pockets are empty")

    def handle_verb_help(self, item, _):
        print("You are currently in a text adventure game ")
        print("If you are stuck, try using the verbs get, look, drop, ")
        print("go, or inventory.  If you have to go, use quit.")
        

    @staticmethod
    def find_item(items, words):
        description = str.join(' ', words)
        description = description.lower()
        if not words:
            return
        found_items = []
        for item in items:
            aliases = [x.lower() for x in item.aliases]
            name = item.name.lower()
            if description in aliases:
                found_items.append(item)
            elif description == name:
                found_items.append(item)
            elif name.startswith(description):
                found_items.append(item)
            elif any([x.startswith(description) for x in aliases]):
                found_items.append(item)
        if len(found_items) == 1:
            return found_items[0]

    def get_item(self, item):
        self.current_room.contents.remove(item)
        self.player.inventory.append(item)

    def drop_item(self, item):
        self.player.inventory.remove(item)
        self.current_room.contents.append(item)

    def go(self, new_room):
        """Move the player to a new room

        This method is called when the user has requested to go to a room
        that is a valid destination from the current room.

        As part of the movement the looked flag is reset so that the room
        description is shown after the player moves into the new room.

        :param new_room: The new room to move to
        :return: None
        """
        self.current_room = new_room
        self.looked = False


def select_adventure():
    from adventure.games import all_games
    if len(all_games) == 1:
        return all_games[0]
    while True:
        print("What game would you like to play?")
        for i, g in enumerate(all_games):
            print("{} - {}".format(i + 1, g.name))
        index = int(input())
        if 1 <= index <= len(all_games):
            return all_games[index - 1]


def main():
    adventure = select_adventure()
    if adventure:
        name = input("What is your name? ")
        p = Player(name)
        adventure.start_adventure(p)


if __name__ == '__main__':
    main()
