"""
Adventure is a text adventure game
Copyright (C) 2017 Chris Beelby <cmbeelby@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from adventure import Adventure, Room, Item, Intro
import random

# Define the intro
intro = Intro(
    "You are trying to go to a Python programming meetup."
)

# Define all the rooms
kitchen = Room(
    "Kitchen",
    "You are in a small kitchen. This room apparently also serves as the "
    "printer room and office supply closet. The faucet drips slowly but "
    "loudly with a brown colored water."
)

conference_room = Room(
    "Conference Room",
    "You are in what seems to be a conference room. There are chairs and "
    "tables arranged in the center of the room with a view of the television "
    "and dry erase board at the north and south ends of the room. There is a "
    "doorway to the west leading to a small kitchen."
)

hall = Room(
    "Hallway",
    "You find yourself at the end of a seemingly endless hallway. The color "
    "of the carpet is painful to behold and you desperately seek an exit from "
    "its overpowering hues. There is an open door to the west. You can also "
    "take the elevator back down to the lobby."
)

basement = Room(
    "Basement",
    "You are in the basement of the high-rise. There is a boiler here, but "
    "since it is summer time it is not running."
)
basement.examined_boiler = False

lobby = Room(
    "Lobby",
    "You find yourself in the lobby of the building. There are three elevator "
    "shafts but only one of them is working. To the south is the exit to the "
    "street."
)
lobby.invited = False


class WorkRoom(Room):
    def __init__(self):
        self.purchased_membership = False
        name = "Shared Work Space"
        long_description = (
            "You are in an open concept shared working "
            "space. Various independent contractors and remote workers use "
            "this space to work.")
        super().__init__(name, long_description)

    def get_long_description(self):
        result = self.long_description
        if not self.purchased_membership:
            result += (
                " One of them introduces himself to you and asks if you "
                "are planning to purchase a membership.")
        else:
            result += (
                " One of them says hello and welcomes you to the shared "
                "work space.")
        return result
work_room = WorkRoom()

street = Room(
    "Jefferson Street",
    "You are standing on the street in front of a building called the "
    "Jefferson Centre. It seems that someone doesn't know how to spell center. "
    "Work crews are in the process of updating the exterior of the building."
)

# Define all the objects
copier = Item("Copy Machine", ['copier', 'machine'])
fridge = Item("Refrigerator", ['fridge'])
fridge.hasCash = True
tv = Item("Westinghouse TV", ['tv', 'westinghouse', 'television'])
tv.you_were_warned = False
strange_drink = Item("12oz Can", ['can'])
cash = Item("Pile of Cash", ['cash', 'money'])

# Place items in their initial rooms
kitchen.contents.extend([copier, fridge])
conference_room.contents.extend([tv, strange_drink])

# Setup connections between rooms
hall.directions['west'] = conference_room
hall.directions['down'] = lobby
conference_room.directions['east'] = hall
conference_room.directions['west'] = kitchen
kitchen.directions['east'] = conference_room
kitchen.directions['north'] = work_room
work_room.directions['south'] = kitchen
work_room.directions['hall'] = hall
lobby.directions['up'] = hall
lobby.directions['down'] = basement
lobby.directions['south'] = street
basement.directions['up'] = lobby

class MeetupAdventure(Adventure):
    def start_adventure(self, player):
        player.drunk = False
        super().start_adventure(player, intro)

    def handle_verb_use(self, item, words):
        if item == cash and self.current_room == work_room and \
                cash in self.player.inventory:
            self.player.inventory.remove(cash)
            work_room.purchased_membership = True
            self.player.points += 50
            Adventure.wrap_message(
                "At the suggestion of one of the other members you decide to "
                "purchase a membership and try it out for a while. That cash "
                "you found earlier should cover a month or two."
            )
        else:
            print("I'm not sure how to do that.")

    def handle_verb_look(self, item, words):
        if not item and not words:
            self.show_full_room_description()
        elif self.current_room == basement and 'boiler' in words and \
                not basement.examined_boiler:
            self.player.points += 10
            Adventure.wrap_message(
                "The boiler looks a little worn out. Looks like someone was "
                "in the process of replacing the electrical components."
            )
            basement.examined_boiler = True
        elif not item:
            print("I'm not sure what you want to look at")
        elif item == tv and not tv.you_were_warned:
            print("The TV looks very heavy. I wouldn't try to move it.")
            self.player.points += 10
            tv.you_were_warned = True
        elif item == fridge and fridge.hasCash:
            print("You find a pile of cash inside.")
            self.current_room.contents.append(cash)
            self.player.points += 50
            fridge.hasCash = False
        elif item == strange_drink:
            print("That looks delicious. You should drink it.")
        else:
            messages = [
                "It doesn't look all that unusual.",
                "Nothing to see here.",
                "That is a rather fascinating thing to look at.",
                "What?",
                "It is what it is."
            ]
            print(random.choice(messages))

    def handle_verb_go(self, item, words):
        if words:
            direction = words[0]
            new_room = self.current_room.directions.get(direction)
            if new_room and not self.player.drunk:
                self.go(new_room)
            elif new_room:
                self.go_drunk(direction)
            else:
                Adventure.wrap_message(
                    "I do not see an exit {} from here".format(direction))
        else:
            print("Go where?")

    def go(self, new_room):
        if self.current_room == lobby and not lobby.invited:
            Adventure.wrap_message(
                "As you are about to leave a good looking bald man with a "
                "beard greets you and invites you to visit the office space "
                "he has available on the 5th floor. Not being able to "
                "remember what you were doing in the first place, you decide "
                "to take him up on his offer for now."
            )
            lobby.invited = True
            self.looked = False
            self.current_room = conference_room
        elif new_room == street:
            Adventure.wrap_message(
                "You suddenly remember what you were doing and decide "
                "to go home."
            )
            self.player.points += 10
            self.game_over = True
        else:
            super().go(new_room)

    def go_drunk(self, intended_direction):
        d = list(self.current_room.directions.keys())
        d.remove(intended_direction)
        d.append(None)
        new_direction = random.choice(d)
        self.player.drunk = random.choice([True, False])
        if not new_direction:
            print("You are having a hard time finding your way out.")
            return
        new_room = self.current_room.directions.get(new_direction)
        Adventure.wrap_message(
            "That drink you had earlier is affecting you more than you "
            "expected. You start to go {} but end up going {} "
            "instead.".format(intended_direction, new_direction)
        )
        self.go(new_room)

    def handle_verb_drink(self, item, words):
        if not item:
            print("What do you want to drink?")
        elif item == strange_drink:
            print("That taste a little funny.")
            self.player.points += 10
            if strange_drink in self.current_room.contents:
                self.current_room.contents.remove(strange_drink)
            else:
                self.player.inventory.remove(strange_drink)
            self.player.drunk = True
        else:
            print("I don't think you should drink that.")

    def get_item(self, item):
        if item == tv:
            print("That looks too heavy to move.")
            print("You try anyways and fall over.")
            print("The TV crushes you to death.")
            self.game_over = True
        elif item in [fridge, copier]:
            Adventure.wrap_message("Seems like it might not be the kind of thing you can "
                  "just carry around with you.")
            if item == fridge:
                print("Maybe you should look inside.")
        else:
            super().get_item(item)


the_meetup = MeetupAdventure("The Meetup", basement)
